Hello
Please note the following points:
1. I have assumed the price to be an Integer, which perhaps is not true in real world. However from the examples in the assignment, it seems that this is an acceptable assumption to make.
As a result, I have used the price as a key in a HashMap (which contains the aggregated quantity as the value). This allows for a running aggregated value to be retained when registerOrder and
removeOrder are called. This avoids having to iterate and aggregate over all orders each time getBoard() is called (assuming that it is called very frequently and assuming that the number of orders is huge).
It's ok to use an Integer Price as the key in the maps as there is no precision issue involved. It would not have been acceptable if price was a double
In which case, I either could not have used the concept of running aggregated quantity per price or i should have used some sort of a bucket price as the key in the Map to accomodate float precision.
So, in such a case, the aggregation would be done as part of the getBoard() method call.

2. I have assumed that multiple threads invoke registerOrder() and removeOrder(), and hence have synchronized the 2 methods on the 'this' instance of LiveOrderBoardServiceImpl.

3. When the quantity for a price bracket goes to 0, I have chosen not to remove the entry from the aggregatedQuantityByPriceSell and aggregatedQuantityByPriceBuy maps. This behaviour can easily be changed if needed.

4. I have chosen to hide the concept of an "Order" from the user of the class. Order class is internal and only the service (and its unit tests) use it. One of the reasons for doing this is to ensure
that each order has a unique id assigned to it at the time of registration. This ensures that whilst removing an order, the search for the order to be removed is o(1) operation on the orders HashMap.



