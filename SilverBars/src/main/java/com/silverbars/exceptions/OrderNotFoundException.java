package com.silverbars.exceptions;

public class OrderNotFoundException extends Exception {
    private final int orderId;

    public OrderNotFoundException(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getReason() {
        return "Order with id " + orderId + " does not exist in the system";
    }
}
