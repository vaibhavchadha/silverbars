package com.silverbars.model;

import java.util.Map;

public class OrderBoard {

    private final Map<Integer, Double> aggregatedQuantityByPriceSell;
    private final Map<Integer, Double> aggregatedQuantityByPriceBuy;

    public OrderBoard(Map<Integer, Double> aggregatedQuantityByPriceSell,
                      Map<Integer, Double> aggregatedQuantityByPriceBuy) {
        this.aggregatedQuantityByPriceBuy = aggregatedQuantityByPriceBuy;
        this.aggregatedQuantityByPriceSell = aggregatedQuantityByPriceSell;
    }

    public Map<Integer, Double> sellSummary() {
        return aggregatedQuantityByPriceSell;
    }

    public Map<Integer, Double> buySummary() {
        return aggregatedQuantityByPriceBuy;
    }

}
