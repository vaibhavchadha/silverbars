package com.silverbars.service;

import com.silverbars.exceptions.OrderNotFoundException;
import com.silverbars.model.OrderBoard;
import com.silverbars.model.Side;

public interface LiveOrderBoardService {

    int registerOrder(String userId, int price, double quantity, Side side);

    int removeOrder(int orderId) throws OrderNotFoundException;

    OrderBoard getBoard();


}
