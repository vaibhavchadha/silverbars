package com.silverbars.service;

import com.silverbars.exceptions.OrderNotFoundException;
import com.silverbars.model.OrderBoard;
import com.silverbars.model.Side;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
    package internal Immutable Order class

 */
final class Order {
    private final String userId;
    private final Side side;
    private final double quantity;
    private final int id;

    //Based on the examples given in the assignment, I treat price as an Integer.
    //In practice, this may not be the case i.e. it would be double.
    private Integer price;

    public Order(String userId, Side side, double quantity, int price, int id) {
        if (side == null)
            throw new IllegalArgumentException("side can not be NULL");
        if (userId == null)
            throw new IllegalArgumentException("userId can not be NULL");
        if (price <= 0)
            throw new IllegalArgumentException("price can not be less than or equal to 0");
        if (quantity <= 0)
            throw new IllegalArgumentException("quantity can not be less than or equal to 0");

        this.id = id;
        this.userId = userId;
        this.side = side;
        this.quantity = quantity;
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public Side getSide() {
        return side;
    }

    public double getQuantity() {
        return quantity;
    }

    public int getId() {
        return id;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Double.compare(order.quantity, quantity) == 0 &&
                id == order.id &&
                userId.equals(order.userId) &&
                side == order.side &&
                price.equals(order.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, side, quantity, id, price);
    }
}


public class LiveOrderBoardServiceImpl implements LiveOrderBoardService {

    final Map<Integer, Double> aggregatedQuantityByPriceSell = new HashMap<>();
    final Map<Integer, Double> aggregatedQuantityByPriceBuy = new HashMap<>();


    //just use id
    final Map<Integer, Order> orders = new HashMap<>();
    private final Function<Side, Map<Integer, Double>> mapBySide = (side) -> side == Side.BUY ? aggregatedQuantityByPriceBuy : aggregatedQuantityByPriceSell;

    private final Comparator<Map.Entry<Integer, Double>> comparatorBuy = Map.Entry.comparingByKey(Comparator.reverseOrder());
    private final Comparator<Map.Entry<Integer, Double>> comparatorSell = Map.Entry.comparingByKey();

    private AtomicInteger idGen = new AtomicInteger(0);

    @Override
    public synchronized int registerOrder(String userId, int price, double quantity, Side side) {
        Order order = new Order(userId, side, quantity, price, idGen.incrementAndGet());
        orders.put(order.getId(), order);
        mapBySide.apply(order.getSide()).compute(order.getPrice(), (k, v) -> v == null ? order.getQuantity() : v + order.getQuantity());
        return order.getId();
    }

    @Override
    public synchronized int removeOrder(int orderId) throws OrderNotFoundException {
        if (!orders.containsKey(orderId)) {
            throw new OrderNotFoundException(orderId);
        }
        //order exists, so remove
        Order order = orders.remove(orderId);
        mapBySide.apply(order.getSide()).compute(order.getPrice(), (k, v) -> v - order.getQuantity());
        return order.getId();
    }


    /*
        1. Retrieves the buy and sell boards and sorts them by key in the right order (Ascending on price for sell and descending on price for buy)
        2. Creates an OrderBoard object and returns it

     */
    @Override
    public OrderBoard getBoard() {
        Map<Integer, Double> sortedSell = aggregatedQuantityByPriceSell.entrySet().stream().sorted(comparatorSell).
                collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue(), (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        Map<Integer, Double> sortedBuy = aggregatedQuantityByPriceBuy.entrySet().stream().sorted(comparatorBuy).
                collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue(), (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        return new OrderBoard(sortedSell, sortedBuy);
    }
}
