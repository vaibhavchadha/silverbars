package com.silverbars.service;


import com.silverbars.exceptions.OrderNotFoundException;
import com.silverbars.model.OrderBoard;
import com.silverbars.model.Side;
import org.junit.Test;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.*;

public class OrderBoardServiceImplTest {
    private static double acceptableDelta = .0001;

    boolean orderEquals(Order order, String user, int price, double quantity, Side side) {
        return order.getPrice().equals(price) &&
                order.getQuantity() == quantity &&
                order.getUserId().equals(user) &&
                order.getSide().equals(side);
    }

    @Test
    public void testAddOrder() {
        LiveOrderBoardServiceImpl impl = new LiveOrderBoardServiceImpl();
        impl.registerOrder("u1", 1, 5.0, Side.BUY);
        impl.registerOrder("u1", 1, 5.4, Side.BUY);
        impl.registerOrder("u1", 2, 10.1, Side.Sell);
        impl.registerOrder("u1", 2, 10.2, Side.Sell);
        impl.registerOrder("u1", 2, 15.0, Side.BUY);

        //the 3 orders should have order ids 1,2 and 3 respectively.
        Order order1 = impl.orders.get(1);
        Order order2 = impl.orders.get(2);
        Order order3 = impl.orders.get(3);
        Order order4 = impl.orders.get(4);
        Order order5 = impl.orders.get(5);

        assertTrue(order1 != null);
        assertTrue(order2 != null);
        assertTrue(order3 != null);
        assertTrue(order4 != null);
        assertTrue(order5 != null);

        assertTrue(orderEquals(order1, "u1", 1, 5.0, Side.BUY));
        assertTrue(orderEquals(order2, "u1", 1, 5.4, Side.BUY));
        assertTrue(orderEquals(order3, "u1", 2, 10.1, Side.Sell));
        assertTrue(orderEquals(order4, "u1", 2, 10.2, Side.Sell));
        assertTrue(orderEquals(order5, "u1", 2, 15.0, Side.BUY));

        assertTrue(impl.aggregatedQuantityByPriceBuy.size() == 2);
        assertTrue(impl.aggregatedQuantityByPriceSell.size() == 1);

        double aggQuantity1 = impl.aggregatedQuantityByPriceBuy.get(1);
        assertEquals(10.4, aggQuantity1, acceptableDelta);

        double aggQuantity2 = impl.aggregatedQuantityByPriceSell.get(2);
        assertEquals(20.3, aggQuantity2, acceptableDelta);

        double aggQuantity3 = impl.aggregatedQuantityByPriceBuy.get(2);
        assertEquals(15.0, aggQuantity3, acceptableDelta);
    }

    @Test
    public void testRemoveOrder() {
        boolean exceptionThrown = false;

        LiveOrderBoardServiceImpl impl = new LiveOrderBoardServiceImpl();
        int retOrderId = 0;
        try {

            impl.registerOrder("u1", 1, 5.0, Side.BUY);
            impl.registerOrder("u2", 1, 4.0, Side.BUY);
            impl.registerOrder("u3", 1, 6.0, Side.Sell);
            impl.registerOrder("u4", 1, 10.0, Side.Sell);
            impl.registerOrder("u5", 2, 10.0, Side.BUY);

            retOrderId = impl.removeOrder(1);
            assertEquals(1, retOrderId);
            assertEquals(4, impl.orders.size());
            assertEquals(2, impl.aggregatedQuantityByPriceBuy.size());
            assertTrue(impl.aggregatedQuantityByPriceBuy.get(1) != null);
            assertEquals(4.0, impl.aggregatedQuantityByPriceBuy.get(1), acceptableDelta);

            retOrderId = impl.removeOrder(2);
            assertEquals(2, retOrderId);
            assertEquals(3, impl.orders.size());
            assertTrue(impl.aggregatedQuantityByPriceBuy.get(1) != null);
            assertEquals(2, impl.aggregatedQuantityByPriceBuy.size());
            assertEquals(0.0, impl.aggregatedQuantityByPriceBuy.get(1), acceptableDelta);

            retOrderId = impl.removeOrder(3);
            assertEquals(3, retOrderId);
            assertEquals(2, impl.orders.size());
            assertTrue(impl.aggregatedQuantityByPriceSell.get(1) != null);
            assertEquals(10.0, impl.aggregatedQuantityByPriceSell.get(1), acceptableDelta);

            retOrderId = impl.removeOrder(4);
            assertEquals(4, retOrderId);
            assertEquals(1, impl.orders.size());
            assertTrue(impl.aggregatedQuantityByPriceSell.get(1) != null);
            assertEquals(0.0, impl.aggregatedQuantityByPriceSell.get(1), acceptableDelta);

            retOrderId = impl.removeOrder(5);
            assertEquals(5, retOrderId);
            assertEquals(0, impl.orders.size());
            assertTrue(impl.aggregatedQuantityByPriceBuy.get(1) != null);
            assertEquals(0.0, impl.aggregatedQuantityByPriceBuy.get(1), acceptableDelta);
            assertTrue(impl.aggregatedQuantityByPriceBuy.get(2) != null);
            assertEquals(0.0, impl.aggregatedQuantityByPriceBuy.get(2), acceptableDelta);
        } catch (OrderNotFoundException ex) {
            exceptionThrown = true;
        }
        assertTrue(!exceptionThrown);
    }

    @Test
    public void testRemoveUnknownOrder() {
        LiveOrderBoardServiceImpl impl = new LiveOrderBoardServiceImpl();
        boolean exceptionThrown = false;
        try {

            impl.registerOrder("u1", 1, 5.0, Side.BUY);
            impl.removeOrder(2);
        } catch (OrderNotFoundException oe) {
            exceptionThrown = true;
            assertEquals(2, oe.getOrderId());
        }
        assertTrue(exceptionThrown);

    }


    @Test
    public void testBuyOrderSummary() {
        LiveOrderBoardServiceImpl impl = new LiveOrderBoardServiceImpl();
        impl.registerOrder("u1", 1, 5.0, Side.BUY);
        impl.registerOrder("u2", 1, 5.4, Side.BUY);
        impl.registerOrder("u3", 2, 10.1, Side.BUY);
        impl.registerOrder("u4", 2, 10.2, Side.BUY);
        impl.registerOrder("u4", 1, 2.2, Side.BUY);
        try {
            impl.removeOrder(5);
        } catch (OrderNotFoundException e) {
            e.printStackTrace();
        }
        OrderBoard board = impl.getBoard();
        assertTrue(board != null);
        assertTrue(board.buySummary() != null);
        assertTrue(board.sellSummary() != null);
        assertTrue(board.sellSummary().size() == 0);
        assertTrue(board.buySummary().size() == 2);

        Map<Integer, Double> buySummary = board.buySummary();
        //keyset.toarray() should retain the order as dictated by the linkedhashmap
        Integer[] actual = buySummary.keySet().toArray(new Integer[buySummary.keySet().size()]);
        Integer[] expected = {2,1};
        assertArrayEquals(expected,actual);
        //Assert that the aggregated values are still correct

        assertEquals(10.4, buySummary.get(1), acceptableDelta);
        assertEquals(20.3, buySummary.get(2), acceptableDelta);

        try {
            impl.removeOrder(4);
        } catch (OrderNotFoundException e) {
            e.printStackTrace();
        }
        board  = impl.getBoard();
        buySummary = board.buySummary();
        assertEquals(10.1, buySummary.get(2), acceptableDelta);
    }

    @Test
    public void testSellOrderSummary() {
        LiveOrderBoardServiceImpl impl = new LiveOrderBoardServiceImpl();
        impl.registerOrder("u1", 1, 5.0, Side.Sell);
        impl.registerOrder("u2", 1, 5.4, Side.Sell);
        impl.registerOrder("u3", 2, 10.1, Side.Sell);
        impl.registerOrder("u4", 2, 10.2, Side.Sell);
        impl.registerOrder("u4", 3, 2.2, Side.Sell);

        OrderBoard board = impl.getBoard();
        assertTrue(board != null);
        assertTrue(board.sellSummary() != null);
        assertTrue(board.sellSummary() != null);
        assertTrue(board.buySummary().size() == 0);
        assertTrue(board.sellSummary().size() == 3);


        Map<Integer, Double> sellSummary = board.sellSummary();
        //keyset.toarray() should retain the order as dictated by the linkedhashmap
        Integer[] actual = sellSummary.keySet().toArray(new Integer[sellSummary.keySet().size()]);
        Integer[] expected = {1,2,3};

        assertArrayEquals(expected, actual);
        assertEquals(10.4, sellSummary.get(1), acceptableDelta);
        assertEquals(20.3, sellSummary.get(2), acceptableDelta);
        assertEquals(2.2, sellSummary.get(3), acceptableDelta);
    }
}
